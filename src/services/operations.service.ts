import { Injectable }              from '@angular/core';
import { Http, Response, Headers, RequestOptions }          from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { User } from '../models/user';


@Injectable()
export class OperationsService {

  private apiUrl = '';
  private ApiKey = '';

  constructor ( private http: Http ) {}

  issueCard(issueInfo:any, user:any):Observable<any>{
    let data =  {
      'ApiKey': this.ApiKey,
      'AccessToken': user.token,
      'actionCode': 10,
      'userInfo': issueInfo.userInfo,
      'merchantId': user.merchantId,
      'merchantLocationId': user.merchantLocationId,
      'amount': issueInfo.amount,
      'salesPersonId': '20',
      'from': issueInfo.from,
      'userMessage': issueInfo.userMessage,
      'campaignId': issueInfo.campaignId,
      'externalReference': issueInfo.externalReference,
    };

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    
    let params  = new URLSearchParams();

    for(let i in data){
      params.append(i,data[i]);
    }

    let options = new RequestOptions({ headers: headers });
    let body = params.toString();

    return this.http.post(this.apiUrl+'sv', body , options)
    .map( (response: Response) => response.json() )
    // .catch(error => error);;
  }

  redeemCard(redeemInfo:any, user:any):Observable<any>{
    let data =  {
      'ApiKey': this.ApiKey,
      'AccessToken': user.token,
      'actionCode': 20,
      'merchantId': user.merchantId,
      'merchantLocationId': user.merchantLocationId,
      'amount': redeemInfo.amount,
      'token': redeemInfo.token,
      'externalReference': redeemInfo.externalReference,
    };

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    
    let params  = new URLSearchParams();

    for(let i in data){
      params.append(i,data[i]);
    }

    let options = new RequestOptions({ headers: headers });
    let body = params.toString();

    return this.http.post(this.apiUrl+'sv', body , options)
    .map( (response: Response) => response.json() )
    // .catch(error => error);;
  }

  getBalance(balanceInfo:any, user:any):Observable<any>{
    let data =  {
      'ApiKey': this.ApiKey,
      'AccessToken': user.token,
      'actionCode': 40,
      'merchantId': user.merchantId,
      'merchantLocationId': user.merchantLocationId,
      'token': balanceInfo.token,
    };

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    
    let params  = new URLSearchParams();

    for(let i in data){
      params.append(i,data[i]);
    }

    let options = new RequestOptions({ headers: headers });
    let body = params.toString();

    return this.http.post(this.apiUrl+'sv', body , options)
    .map( (response: Response) => response.json() )
    // .catch(error => error);;
  }


  
}
