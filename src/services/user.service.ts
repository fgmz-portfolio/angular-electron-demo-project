import { Injectable }              from '@angular/core';
// import { Http, Response, Headers, RequestOptions }          from '@angular/http';

// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/map';

// import { User } from '../models/user';


@Injectable()
export class UserService {
  userStored:any;

  constructor () {}

  getUser (){
    let savedUser = window.localStorage.getItem('currentUser');
    if( savedUser ){
      this.userStored = JSON.parse(savedUser) ;  
    }
    return this.userStored;
  }

  setUser (userObj){
    window.localStorage.setItem('currentUser',JSON.stringify(userObj) );
    this.userStored = this.getUser();
    console.log('userStored: ', this.userStored);
  }

  isUser(){
    return this.userStored !== undefined;
  }

}