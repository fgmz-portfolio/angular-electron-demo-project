export class User {
	public token: string;
    public name: string;
    public firstName: string;
    
    public expire: string;
    public userId: string;
    public image: string;
    public fullImageUrl: string;
    public merchantId: string;
    public merchantLocationId: string;
    public merchantName: string;
    public phone: string;
    public email: string;
    public pin: string;

    constructor( private user: any){
        for(let attribute in user){
            this[attribute] = user[attribute];
        }
    }


    // isLoggedIn(user){
    // 	return (user.token != "");
    // }
    
}
