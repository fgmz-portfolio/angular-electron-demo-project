import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
    moduleId: module.id,
    selector: 'app-root',
    templateUrl: 'root.component.html'
})
export class RootComponent{
	constructor (
			private router: Router, 
			public userService: UserService
		){
		console.log(' saved user?:', this.userService.getUser());
		console.log(this.userService.getUser());

		if(!this.userService.isUser()){
			this.router.navigate(['/']);
		}
	}
}
