import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { UserService } from '../../services/user.service';
import { OperationsService } from '../../services/operations.service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
  })
export class BalanceComponent implements OnInit {

  isFormDisabled: Boolean = false;
  isValidating: Boolean = false;
  balanceDisplayed: Boolean = false;
  public user: any = this.userService.getUser();
  public balanceModel = {
    token: '',
    amount: 0
  };

  constructor(
    private router: Router, 
    public snackBar: MdSnackBar,
    private userService: UserService,
    private operationsService: OperationsService
  ) { }

  ngOnInit() {
    console.log('balanceDisplayed ',this.balanceDisplayed);
    this.initializeModel();
  }

  initializeModel(){
    this.balanceModel = {
      token: '',
      amount: 0
    };

    this.isFormDisabled = false;
    this.balanceDisplayed = false;
    console.log('balance reinitialized');
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  goHome(){
    this.router.navigate(["/home"]);
  }

  save(){
    this.isFormDisabled = true;
    this.isValidating = true;


    this.operationsService.getBalance(this.balanceModel, this.user)
    .subscribe( response => {
      let r = response;
      console.log('issue response', r );

      if(r.responseCode !== 100 || r.success !== 1){
        console.error('Get Balance error: '+ r.message);
        

        this.isValidating = false;
        this.isFormDisabled = false;
        let snackBarRef = this.snackBar.open(
          'Get Balance error. '+r.message,
          '',
          { 
            duration: 6000,
            extraClasses: ['accent']
          }
        );

      }else{
        
        this.isValidating = false;
        this.balanceDisplayed = true;
        this.balanceModel.amount = response.result.balance;

        let snackBarRef = this.snackBar.open('Balance is been shown!!','',{duration : 5000});
        snackBarRef.afterDismissed().subscribe(() => {
          this.initializeModel();  

        });
        
      }
    });

    // let timeoutId = setTimeout(() => {  
    //  clearTimeout(timeoutId);
    //  this.isValidating = false;

    //  if(this.getRandomInt(0,1) == 0){
    //    // if its 0 then proceed
       
    //    this.balanceDisplayed = true;
    //    console.log('balanceDisplayed!');

    //  }else{
    //    this.isFormDisabled = false;
    //    let snackBarRef = this.snackBar.open(
    //      'Balance Unavailable, please try again later',
    //      '',
    //      { 
    //        duration: 3000,
    //        extraClasses: ['accent']
    //      }
    //    );
    //  }

    //  }, 2000);



  }

}
