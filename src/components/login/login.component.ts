import { Component, OnInit, HostBinding, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import { LoginService } from '../../services/login.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  // animations: [slideInDownAnimation]
})
export class LoginComponent implements OnInit {
  public userModel = {
    'username': '',
    'password': ''
  };
	// @HostBinding('@routeAnimation') routeAnimation = true;
	// @HostBinding('style.display')   display = 'block';
	// @HostBinding('style.position')  position = 'absolute';

  constructor(
    private router: Router, 
    private loginService: LoginService,
    private userService: UserService,
    public snackBar: MdSnackBar
  ) { 
    this.loginService.getUser()
    .subscribe( user => console.log('mocked user', user) );

    
    
  }

  ngOnInit() {
  }

  login() {

  	console.log("login",this.userModel);

    this.loginService.doLogin(this.userModel.username, this.userModel.password)
    .subscribe( response => {
      let r = response;
      console.log('login response', r );
      if(r.responseCode !== 100 || r.success !== 1){
        console.error('Login error: '+ r.message);
        
        let snackBarRef = this.snackBar.open(
          'Error: '+r.message,
          '',
          { 
            duration: 3000,
            extraClasses: ['accent']
          }
        );
      }else{
        this.snackBar.dismiss();
        console.log('login sucess');
        this.userService.setUser(response);
        this.router.navigate(['/home']);
      }
      
    })
    ;

  	
  }

}
